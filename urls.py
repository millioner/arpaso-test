# -*- coding: utf-8 -*-
from django.conf import settings
from django.contrib import admin
from django.conf.urls.defaults import *
from profile.views import *

admin.autodiscover()

urlpatterns = patterns('profile.views',
    url(r'^$', 'main_page', name="main_page"),
    url(r'^account/', 'edit_profile', name="edit_page")
)

urlpatterns += patterns('',
    url(r'^login/$', 'django.contrib.auth.views.login', name='login_page'),
    url(r'^logout/$', 'django.contrib.auth.views.logout_then_login', name='logout_page'),
    (r'^admin/', include(admin.site.urls)),
)

if settings.DEBUG:
    urlpatterns += patterns('',
        (r'', include('staticfiles.urls')),
    )
