# -*- coding: utf-8 -*-
from datetime import datetime
from django.db import models
from django.db.models.signals import post_save, post_delete
from django.contrib.auth.models import User


class UserProfile(models.Model):
    """
    Model for adding extended fields to the User object
    """
    user = models.ForeignKey(User, unique=True)
    birthdate = models.DateField(blank=True, null=True)
    biography = models.TextField(verbose_name=u'Biography', blank=True, null=True)
    contacts = models.TextField(verbose_name=u'Contacts', blank=True, null=True)
    class Meta:
        verbose_name = u'Additional info'
        verbose_name_plural = u'Additional info'
    def __unicode__(self):
        return self.user.get_full_name()

class dbLog(models.Model):
    """
    Model for logging all changes
    """
    time = models.DateTimeField(auto_now=True)
    model_name = models.CharField(max_length=255)
    action = models.CharField(max_length=25)

def log_db_deletes(sender, instance, **kwargs):
    """
    Function called every time when User or UserProfile deleted
    For saving some notes about this
    """
    log_unit = dbLog()
    log_unit.time = datetime.now()
    log_unit.model_name = sender
    log_unit.action = 'deleted'
    log_unit.save()

def log_db_changes(sender, instance, created, **kwargs):
    """
    Function called every time when User or UserProfile saved
    For saving some notes about this
    """
    log_unit = dbLog()
    log_unit.time = datetime.now()
    log_unit.model_name = sender.__name__
    if created:
        log_unit.action = 'added'
    else:
        log_unit.action = 'changed'
    log_unit.save()

post_save.connect(log_db_changes, sender=User)
post_save.connect(log_db_changes, sender=UserProfile)
post_delete.connect(log_db_deletes, sender=User)
post_delete.connect(log_db_deletes, sender=UserProfile)
