# -*- coding: utf-8 -*-
import os
from django import template
from django.core import urlresolvers
from django.contrib.contenttypes.models import ContentType


register = template.Library()

@register.filter
def get_edit_link(data, arg=False):
    content_type = ContentType.objects.get_for_model(data.__class__)
    return urlresolvers.reverse(
                "admin:%s_%s_change" % (content_type.app_label, content_type.model), args=(data.id,)
           )
