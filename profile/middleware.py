import logging
import os
from datetime import datetime
from django.db.backends import util
from django.utils import simplejson
from django.utils.encoding import force_unicode
from django.conf import settings


# it is a little issue here - debug toolbar does not work if this stuff is included in the project

class DatabaseStatLogger(util.CursorDebugWrapper):
    """
    Replacement for CursorDebugWrapper which stores additional information
    in `connection.queries`.
    """
    def execute(self, sql, params=()):
        try:
            return super(self.__class__, self).execute(sql, params)
        finally:
            _params = ''
            try:
                _params = simplejson.dumps([force_unicode(x, strings_only=True) for x in params])
            except TypeError:
                pass # don't care (object not JSON serializable)

            logging.basicConfig(filename=os.path.join(settings.PROJECT_DIR, 'db.log'), level=logging.DEBUG)
            logging.debug("\nQuery: %s\nparams: %s" % (sql, _params))


# hm, it is really no needs for middleware in this case
# but any way... task description requires it.
class DbLoggingMiddleware(object):
    """
    It is really unneaded middleware class for db query logging
    """
    def process_request(self, request):
        util.CursorDebugWrapper = DatabaseStatLogger


    def process_response(self, request, response):
        return response
