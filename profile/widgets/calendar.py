from django.forms.widgets import Input
from django.template.loader import get_template
from django.template.context import Context
from django.utils.encoding import force_unicode
from django.utils.safestring import mark_safe
from django.forms.util import flatatt

class datePicker(Input):
    """
    Widget class for displaying nice jQueryUI datepicker
    """
    class Media:
        """
        All needed styles and scripts
        """
        css = { 'all': ('css/ui-lightness/jquery-ui-1.8.6.custom.css', ) }
        js = ('js/jquery-1.4.2.min.js', 'js/jquery-ui-1.8.6.custom.min.js')

    def render(self, name, value, attrs=None):
        """
        Lets make html!
        """
        if value is None:
            value = ''
        final_attrs = self.build_attrs(attrs, type=self.input_type, name=name)
        if value != '':
            # Only add the 'value' attribute if a value is non-empty.
            final_attrs['value'] = force_unicode(self._format_value(value))
        return mark_safe(
                u'<script>$(function(){$("#%s").datepicker({dateFormat: "yy-mm-dd"});});</script><input%s />'
                % (final_attrs.get('id'), flatatt(final_attrs), )
            )
        template = get_template('profile/calendar_widget.html')
        c = Context(attrs)
        return template.render(c)

    def value_from_datadict(self, data, files, name):
        """
        Getting a result
        """
        return data.get(name)