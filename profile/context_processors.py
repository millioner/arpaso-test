from django.conf import settings

def get_django_settings(request):
    return { 'django_settings': settings }
