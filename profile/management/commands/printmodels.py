from django.core.management.base import BaseCommand, CommandError
from django.db import connection

class Command(BaseCommand):
    """
    class for implementing Django console command for current app
    so "./manage.py printmodels" returns a list of models in the project
    and object count for each model
    """
    args = ''
    help = 'Prints all models and object counts'

    def handle(self, *args, **options):
        """
        go-go-go lets get all models and object counts
        """
        tables = connection.introspection.table_names()
        seen_models = connection.introspection.installed_models(tables)
        for model in seen_models:
            print(model.__name__ + ": " + str(model.objects.count()))
