# -*- coding: utf-8 -*-
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from profile.models import *

class profileInline(admin.StackedInline):
    model = UserProfile
    fk_name = 'user'
    max_num = 1

class deseegnUserAdmin(UserAdmin):
    inlines = [
        profileInline,
    ]
    list_display = ('username', 'email', 'first_name', 'last_name', 'is_staff', 'is_active')

admin.site.unregister(User)
admin.site.register(User, deseegnUserAdmin)