# -*- coding: utf-8 -*-
from django.template import RequestContext
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib import messages
from profile.models import *
from profile.forms import accountForm, profileForm

@login_required
def main_page(request):
    """
    Main page - displays users and data.
    Hm, can use generic view in this case,
        but it would be so unusual for main page...
    """
    users = User.objects.all()
    return render_to_response('profile/main_page.html',
                              locals(),
                              context_instance=RequestContext(request))
@login_required
def edit_profile(request):
    """
    Propfile edit page - user can change his profile information
    """
    if request.method == 'POST':
        user_form = accountForm(request.POST, instance=request.user)
        profile_form = profileForm(request.POST, instance=request.user.get_profile())
        if user_form.is_valid() and profile_form.is_valid():
            user_form.save()
            profile_form.save()
            messages.success(request, 'Account details updated.')
            return HttpResponseRedirect(reverse('main_page'))
    else:
        user_form = accountForm(instance=request.user)
        profile_form = profileForm(instance=request.user.get_profile())
    return render_to_response('profile/edit_profile.html',
                              locals(),
                              context_instance=RequestContext(request))