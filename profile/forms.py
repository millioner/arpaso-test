from django.forms import ModelForm
from django.contrib.auth.models import User
from profile.models import UserProfile
from profile.widgets.calendar import datePicker


class accountForm(ModelForm):
    class Meta:
        model = User
        fields = ('last_name', 'first_name', )


class profileForm(ModelForm):
    class Meta:
        model = UserProfile
        fields = ('contacts', 'biography', 'birthdate')
        widgets = {
            'birthdate': datePicker()
        }